﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DesafioHackathon.Migrations
{
    public partial class IncludeImageUrlMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ImageUrl",
                table: "Carta",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ImageUrl",
                table: "Carta");
        }
    }
}
