﻿using DesafioHackathon.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DesafioHackathon.Controllers
{
    public class CartaController : Controller
    {
        private readonly string _Url = string.Format("https://localhost:44309");
        private readonly string _UrlApi = string.Format("/api/CartaService/");

        // GET: Carta
        public async Task<ActionResult> Index()
        {
            try
            {
                List<Carta> carta = new List<Carta>();

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(_Url);
                    client.DefaultRequestHeaders.Clear();

                    var retorno = await client.GetAsync(_UrlApi + "ObterCarta");

                    if (retorno.IsSuccessStatusCode)
                    {
                        var result = retorno.Content.ReadAsStringAsync().Result;
                        carta = JsonConvert.DeserializeObject<List<Carta>>(result);
                    }
                }

                return View(carta);
            }
            catch (Exception ex)
            {
                return View(ex.Message);
            }
        }

        // GET: Carta/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Carta/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Carta/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Carta carta)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Carta/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Carta/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Carta/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Carta/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}